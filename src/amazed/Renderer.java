package amazed;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Renderer extends JPanel {

	private static final long serialVersionUID = 64641546;
	private int blockHeight = 15;
	private int blockWidth = blockHeight;
	private JFrame frame;
	private Maze maze;

	public Renderer(Maze maze) {
		this.maze = maze;
		this.setBackground(Color.white);
		frame = new JFrame();
		frame.setSize((maze.getcolums() + 1) * blockWidth, (maze.getRows() + 2) * blockHeight);
		frame.setContentPane(this);
		
		frame.validate();
		frame.repaint();
		frame.setVisible(true);
		setFocusable(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void setBlockHeight(int a) {
		blockHeight = a;
	}
	

	public void paintComponent(Graphics g) {
		if (maze.isFound()) {
			super.paintComponent(g);
			// draw the maze row by row
			int xcoordinate = 0;
			int ycoordinate = 0;
			int[][] mazeArray = this.maze.getMazeArray();
			for (int i = 0; i < this.maze.getRows(); i++) {
				for (int j = 0; j < this.maze.getcolums(); j++) {
					if (mazeArray[i][j] == 1) {
						g.setColor(Color.darkGray);
						g.fillRect(xcoordinate * blockWidth, ycoordinate * blockHeight, this.blockWidth, this.blockHeight);
						g.setColor(Color.WHITE);
						g.drawRect(xcoordinate * blockWidth, ycoordinate * blockHeight, this.blockWidth, this.blockHeight);
					}
					xcoordinate++;
				}
				xcoordinate = 0;
				ycoordinate++;
			}

			// draw starting point
			g.setColor(Color.green);
			g.fillRect(maze.findStart().getCoord()[0] * blockWidth, maze.findStart().getCoord()[1] * blockHeight,
					this.blockWidth, this.blockHeight);
			
			// draw ending point
			g.setColor(Color.blue);
			g.fillRect(maze.findEnd().getCoord()[0] * blockWidth, maze.findEnd().getCoord()[1] * blockHeight,
					this.blockWidth, this.blockHeight);
			
			// draw the path between start and end point
			if (!maze.getWalked().isEmpty()) {
				g.setColor(Color.red);
				for (int[] cell : maze.getWalked()) {
					g.fillRect(cell[0] * blockWidth, cell[1] * blockHeight, this.blockWidth, this.blockHeight);
				}
			}

			if (!maze.getNodes().isEmpty()) {
				g.setColor(Color.GRAY);
				for (NodesOfMaze node : maze.getNodes()) {
					g.fillRect(node.getCoord()[0] * blockWidth, node.getCoord()[1] * blockHeight, blockWidth,
							blockHeight);
				}
			}

			// draw cursor
			g.setColor(Color.PINK);
			g.fillRect(maze.getCursor()[0] * blockWidth, maze.getCursor()[1] * blockHeight, this.blockWidth,
					this.blockHeight);

			if (!maze.getShortestWay().isEmpty() && maze.isFound()) {
				g.setColor(Color.GREEN);
				for (int[] node : maze.getShortestWay()) {
					g.fillRect(node[0] * blockWidth, node[1] * blockHeight, blockWidth, blockHeight);
				}
			}

		}
	}
}
