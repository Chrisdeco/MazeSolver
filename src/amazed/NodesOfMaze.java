package amazed;

public class NodesOfMaze {
	private double toExit;
	private GetDirection direction;
	private int[] coordinates = new int[2];

	public NodesOfMaze() {
	}

	public NodesOfMaze(int[] coord) {
		this.coordinates = coord;
	}

	public GetDirection getDirection() {
		return direction;
	}

	public void setDirection(GetDirection direction) {
		this.direction = direction;
	}

	public double getToExit() {
		return toExit;
	}

	public void setToExit(double toExit) {
		this.toExit = toExit;
	}

	public int[] getCoord() {
		return coordinates;
	}

	public void setCoord(int[] coord) {
		this.coordinates = coord;
	}

}
