package amazed;

import java.awt.*;
import java.awt.Font;
import java.awt.event.*;

import javax.swing.*;

public class MainGUI extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private MazeGenerator generator;
	private JPanel panel = new JPanel();
	
	private JLabel grootte = new JLabel("grootte");
	private JTextField txtFieldgrootte = new JTextField();

	private JButton generateButton = new JButton("Generate and solve Maze");
	private JLabel errorMessage = new JLabel("Input was not a positive integer!");

	public MainGUI() {

		setTitle("MazeCreator");
		setBounds(250, 250, 600, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		Font font = new Font("arial", Font.BOLD, 22);

		grootte.setBounds(30, 10, 100, 15);
		txtFieldgrootte.setBounds(100, 10, 50, 15);

		generateButton.addActionListener(this);
		generateButton.setBounds(200, 10, 300, 20);

		errorMessage.setBounds(10, 80, 500, 25);
		errorMessage.setVisible(false);
		errorMessage.setFont(font);
		errorMessage.setForeground(Color.RED);
		add(panel);
		panel.add(txtFieldgrootte);

		panel.add(generateButton);
		panel.add(grootte);

		panel.add(errorMessage);
		panel.setLayout(null);
		setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		String textFieldgrootte = txtFieldgrootte.getText();

		if (isInt(textFieldgrootte) == true) {
			errorMessage.setVisible(false);
			int a = Integer.parseInt(textFieldgrootte);
			if (a <= 0) {
				errorMessage.setVisible(true);
			} else {
				generator = new MazeGenerator(a);
				generator.generate();
				Maze doolhof = new Maze();
				doolhof.main(null);

			}

		} else {
			errorMessage.setVisible(true);
		}
	}

	public boolean isInt(String s) {

		try {
			int i = Integer.parseInt(s);
			return true;
		}

		catch (NumberFormatException er) {
			return false;
		}
	}

	public static void main(String[] args) {
		new MainGUI();
	}
}