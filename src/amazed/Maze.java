package amazed;

import java.util.ArrayList;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.io.BufferedReader;

public class Maze {
	private int rows;
	private int colums;
	private Renderer Renderer;
	private boolean found = false;
	private ArrayList<int[]> walked = new ArrayList<>();
	private ArrayList<int[]> shortestRoute = new ArrayList<>();
	private ArrayList<NodesOfMaze> nodes = new ArrayList<>();
	private int[][] mazeArray;
	private int[] cursor = new int[] { 0, 0 };
	
	
	public Maze() {
		this.mazeArray = this.getMazeArray();
		Renderer = new Renderer(this);
	}

	public boolean isFound() {
		return found;
	}

	public void setFound(boolean found) {
		this.found = found;
	}

	public int[] getCursor() {
		return cursor;
	}

	public void setCursor(int[] cursor) {
		this.cursor = cursor;
	}

	public int[] getDim() {
		int[] dim = new int[] { colums, rows };
		return dim;
	}

	public int getRows() {
		return this.rows;
	}

	public int getcolums() {
		return this.colums;
	}

	public ArrayList<int[]> getShortestWay() {
		return shortestRoute;
	}

	public void setShortest(ArrayList<int[]> shortest) {
		this.shortestRoute = shortest;
	}

	public ArrayList<int[]> getWalked() {
		return this.walked;
	}

	public boolean checkFree(int[] coord) {
		if (mazeArray[coord[1]][coord[0]] == 0) {
			return true;
		} else {
			return false;
		}
	}

	// finds the entry point of the maze
	public NodesOfMaze findStart() {
		NodesOfMaze start = new NodesOfMaze();
		for (int i = 0; i < rows; i++) {
			if (mazeArray[i][0] == 0) {
				start.setCoord(new int[] { 0, i });
				break;
			}
		}
		return start;
	}

	// finds the exit point of the maze
	public NodesOfMaze findEnd() {
		NodesOfMaze end = new NodesOfMaze();
		for (int i = 0; i < rows; i++) {
			if (mazeArray[i][colums - 1] == 0) {
				end.setCoord(new int[] { colums - 1, i });
				break;
			}
		}
		return end;
	}

	public int[][] getMazeArray() {

		/*
		 * Do not forget to adapt this to your own file location or else the program
		 * will not work properly
		 */

		String FILENAME = "D:\\Users\\Chris\\eclipse-workspace\\MazeGenerator\\maze.txt";
		String mazeString = "";
		BufferedReader br = null;
		FileReader fr = null;
		int[][] mazeArray;

		try {
			fr = new FileReader(FILENAME);
			br = new BufferedReader(fr);
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				colums = sCurrentLine.length();
				rows++;
				mazeString += (sCurrentLine + System.lineSeparator());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		mazeArray = new int[rows][colums];
		Scanner scanner = new Scanner(mazeString);
		int i = 0;
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();

			for (int j = 0; j < colums; j++) {
				char c = line.charAt(j);
				if ( c == 'X') {
					mazeArray[i][j] = 1;
				} else {
					mazeArray[i][j] = 0;
				}
			}
			i++;
		}
		scanner.close();
		return mazeArray;
	}

	public boolean walkedContains(int[] coords) {
		for (int i = 0; i < walked.size(); i++) {
			int[] cursor = walked.get(i);
			if ((cursor[0] == coords[0]) && (cursor[1] == coords[1])) {
				return true;
			}
		}
		return false;
	}

	public boolean nodesContains(int[] coords) {
		for (int i = 0; i < nodes.size(); i++) {
			NodesOfMaze cursor = nodes.get(i);
			if ((cursor.getCoord()[0] == coords[0]) && (cursor.getCoord()[1] == coords[1])) {
				return true;
			}
		}
		return false;
	}

	public NodesOfMaze returnNode(int[] coords) {
		NodesOfMaze returnNode = new NodesOfMaze();
		for (int i = 0; i < nodes.size(); i++) {
			NodesOfMaze cursor = nodes.get(i);
			if ((cursor.getCoord()[0] == coords[0]) && (cursor.getCoord()[1] == coords[1])) {
				returnNode = cursor;
				break;
			}
		}
		return returnNode;
	}

	public void deleteLast(int[] coords) {
		while (!(shortestRoute.get(0)[0] == coords[0] && shortestRoute.get(0)[1] == coords[1])) {
			shortestRoute.remove(0);
		}
	}

	public void printNodes() {
		for (NodesOfMaze node : nodes) {
			System.out.println("Coords: [" + node.getCoord()[0] + "," + node.getCoord()[1] + "]");
			System.out.println("GetDirection to go: " + node.getDirection());
			System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		}
	}

	public void printshortestRoute() {
		for (int[] node : shortestRoute) {
			System.out.println("Coords: [" + node[0] + "," + node[1] + "]");
		}
	}

	public void walkMaze() {
		cursor = this.findStart().getCoord();

		nodes.add(0, new NodesOfMaze(new int[] { cursor[0], cursor[1] }));
		nodes.get(0).setDirection(GetDirection.Right);

		shortestRoute.add(0, cursor);
		walked.add(0, cursor);
		cursor[0] += 1;
		while (!found) {
			int unvisitedCells = 0;
			int cx = cursor[0];
			int cy = cursor[1];
			if (cx == this.findEnd().getCoord()[0] && cy == this.findEnd().getCoord()[1]) {

				found = true;
				break;
			}

			shortestRoute.add(0, cursor);
			walked.add(0, cursor);
			int[] left = new int[] { cx - 1, cy };
			int[] right = new int[] { cx + 1, cy };
			int[] up = new int[] { cx, cy - 1 };
			int[] down = new int[] { cx, cy + 1 };

			try {
				if (checkFree(left) && !walkedContains(left)) {

					unvisitedCells++;
				}
				if (checkFree(right) && !walkedContains(right)) {

					unvisitedCells++;
				}
				if (checkFree(up) && !walkedContains(up)) {

					unvisitedCells++;
				}
				if (checkFree(down) && !walkedContains(down)) {

					unvisitedCells++;
				}

				if (unvisitedCells >= 1) {
					if (nodesContains(cursor) == false && unvisitedCells > 1) {
						nodes.add(0, new NodesOfMaze(cursor));
					}

					if (checkFree(right) && walkedContains(right) == false) {

						if (nodesContains(cursor)) {
							nodes.get(0).setDirection(GetDirection.Right);
						}
						cursor = right;
					} else if (checkFree(down) && walkedContains(down) == false) {

						if (nodesContains(cursor)) {
							nodes.get(0).setDirection(GetDirection.Down);
						}
						cursor = down;
					} else if (checkFree(up) && walkedContains(up) == false) {

						if (nodesContains(cursor)) {
							nodes.get(0).setDirection(GetDirection.Up);
						}
						cursor = up;
					} else if (checkFree(left) && walkedContains(left) == false) {

						if (nodesContains(cursor)) {
							nodes.get(0).setDirection(GetDirection.Left);
						}
						cursor = left;
					}
				}

				if (unvisitedCells == 0) {

					if (deadEnd(cursor)) {
						cursor = nodes.get(0).getCoord();
						deleteLast(nodes.get(0).getCoord());
					} else {

						deleteLast(nodes.get(0).getCoord());
						nodes.remove(0);
						cursor = nodes.get(0).getCoord();
						deleteLast(nodes.get(0).getCoord());
						
					}
				}

			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
			}

		}
	}
	// checks if a certain spot where te cursor is located in the maze is a dead end by checking the surrounding "boxes" in all 4 directions
	public boolean deadEnd(int[] cursor) {
		int walls = 0;
		int cx = cursor[0];
		int cy = cursor[1];
		int[] left = new int[] { cx - 1, cy };
		int[] right = new int[] { cx + 1, cy };
		int[] up = new int[] { cx, cy - 1 };
		int[] down = new int[] { cx, cy + 1 };
		if (!checkFree(left)) {
			walls++;
		}
		if (!checkFree(right)) {
			walls++;
		}
		if (!checkFree(up)) {
			walls++;
		}
		if (!checkFree(down)) {
			walls++;
		}
		if (walls == 3) {
			return true;
		} else {
			return false;
		}
	}

	public ArrayList<NodesOfMaze> getNodes() {
		return nodes;
	}

	public void setNodes(ArrayList<NodesOfMaze> nodes) {
		this.nodes = nodes;
	}

	public static void main(String[] args) {
		Maze maze = new Maze();
		maze.Renderer.repaint();
		System.out.println("start solving maze");
		long startTijd = System.currentTimeMillis();
		maze.walkMaze();
		long endTijd = System.currentTimeMillis();
		System.out.println("Finding the exit took " + (endTijd - startTijd) + " milliseconds.");
		maze.Renderer.repaint();
	}
}
